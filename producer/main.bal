import ballerina/io;
import ballerina/log;
import ballerinax/kafka;
kafka:Producer kafkaProducer = check new (kafka:DEFAULT_URL);

public string jsonFilePath = "StorageSystem.json";
public json files = {
    "message1": "I love milk and cookies",
    "message2": "You love pizza and coke",
    "message3": "She loves programming in ballerina"
};

public function main() returns error? {
    io:println("Please select wheter you want to read or write files\nEnter either 'read' or 'write'");

    string functionChoice = io:readln("");

    if (functionChoice === "read") {
        boolean test = true;  
        while test{
            io:println("Would you like to read a file?\nEnter 'yes' or 'no'");
            string choice2 = io:readln("");

        
            if (choice2 === "yes") {
                map <json> mFiles = <map<json>> files;
                io:println("Choose a file to read from 1,2 and 3");
                string choice = io:readln("");

                string message1 = mFiles["message1"].toString();
                string message2 = mFiles["message2"].toString();
                string message3 = mFiles["message3"].toString();
                string choiceMessage = "";
                match choice {
                    "1" => {
                        choiceMessage = message1;
                    }
                    "2" => {
                        choiceMessage = message2;
                    }
                    "3" => {
                        choiceMessage = message3;
                    }
                }
                //string message = io:readln("Enter a message: ");
                string message = choiceMessage;

                check kafkaProducer->send({
                                    topic: "read-file",
                                    value: message.toBytes() });
                                    //Acknowledgment
                                    log:printInfo("Message sent succesfully");
            } else {
                check kafkaProducer->'flush();
                test = false;
            }
        }
    } else if (functionChoice === "write") {
        boolean test = true;
        while test {
            io:println("Would you like to read a file?\nPlease enter 'yes' or 'no'");
            string choice2 = io:readln("");

            if (choice2 === "yes") {
                map <json> mFiles = <map<json>> files;
                io:println("Enter the name of the file you want to write.");
                string fileName = io:readln("");
                io:println("Enter information to the file.");
                string fileData = io:readln("");
                mFiles[fileName] = fileData;
                files = mFiles.toJson();
                string writeMessage = fileName + " file created.";

                check io:fileWriteJson(jsonFilePath, files);
                json readJson = check io:fileReadJson(jsonFilePath);

                check kafkaProducer->send({
                                        topic: "write-file",
                                        value: writeMessage.toBytes() });
                                        //Acknowledgment
                                        log:printInfo("File successfully writen.");
            }
            else {
                check kafkaProducer->'flush();
                test = false;
            }
        }
        
    } else {
        io:println("Invalid choice. Please select 'read' or 'write'.");
    }
}